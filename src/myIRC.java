import java.io.IOException;
import java.net.*;
import java.util.Scanner;

/**
 * Created by RENT on 2017-06-22.
 */
public class myIRC {

    final public static String nick = "kruczek";

    public static String getNick() {
        return nick;
    }

    public static void main(String[] args) {

        try {
            DatagramSocket socket = new DatagramSocket(4446);
            myIRCThread myIRCThread = new myIRCThread(socket);
            myIRCThread.start();
            byte[] buf = new byte[1024];

            DatagramPacket packet = new DatagramPacket(buf, buf.length, InetAddress.getByName("10.0.0.255"), 4446);
            Scanner sc = new Scanner(System.in);
            String myMsg = null;

            while ((myMsg = sc.nextLine()) != null) {
                myMsg = "[" + nick + "] " + myMsg;
                packet.setData((myMsg).getBytes());
                socket.send(packet);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Scanner;

/**
 * Created by RENT on 2017-06-22.
 */
public class Server {

    public static void main(String[] args) {

        try{
            DatagramSocket socket = new DatagramSocket(4446);
            byte[] buf = new byte[1024];
            DatagramPacket packet = new DatagramPacket(buf, buf.length);

            Thread server = new Thread(new Runnable() {
                @Override
                public void run() {
                    while (true){
                        try {
                            socket.receive(packet);
                        } catch (IOException e) {e.printStackTrace();}
                        String receive = new String(packet.getData(),0, packet.getLength());
                        System.out.println(receive);
                    }
                }
            });
            server.start();

            Thread client = new Thread(new Runnable() {
                @Override
                public void run() {
                    try{
                        DatagramSocket socket = new DatagramSocket(4445);
                        byte[] buf = new byte[1024];
                        DatagramPacket packet = new DatagramPacket(buf, buf.length, InetAddress.getByName("10.0.0.255"), 4446);
                        Scanner sc = new Scanner(System.in);
                        while (true){
                            String msg = sc.nextLine();
                            packet.setData((msg).getBytes());
                            socket.send(packet);

                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            client.start();


        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}

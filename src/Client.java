import java.io.IOException;
import java.net.*;
import java.util.Scanner;

/**
 * Created by RENT on 2017-06-22.
 */
public class Client {

    public static void main(String[] args) {

        try{
            DatagramSocket socket = new DatagramSocket(4445);
            byte[] buf = new byte[1024];
            DatagramPacket packet = new DatagramPacket(buf, buf.length, InetAddress.getByName("10.0.0.255"), 4446);
            Scanner sc = new Scanner(System.in);
          while (true){
              String msg = sc.nextLine();
              packet.setData((msg).getBytes());
              socket.send(packet);

          }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

/**
 * Created by RENT on 2017-06-22.
 */
public class myIRCThread extends Thread {

    private DatagramSocket socket;

    public myIRCThread(DatagramSocket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        while (true) {
            try {
                byte[] buf = new byte[1024];
                DatagramPacket packet = new DatagramPacket(buf, buf.length);
                this.socket.receive(packet);
                String receive = new String(packet.getData(), 0, packet.getLength());
                if (!receive.startsWith("[" + myIRC.getNick())) {
                    System.out.println(receive);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
